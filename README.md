## Project Information
This is the .net core console application which uses binance cryptocurrency exchange open API via HTTP requests.  
The application first gets data about available trading pairs, and then offers to choose one.  
After that it gets real time data about recent trades of the selected trading pair.