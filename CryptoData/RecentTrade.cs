﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoData
{
    class RecentTrade
    {
        public string pair { get; set; }
        public double price { get; set; }
        public double amount { get; set; }
        public long time { get; set; }
        public bool isBuyerMaker { get; set; }
    }
}
