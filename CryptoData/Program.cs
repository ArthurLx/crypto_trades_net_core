﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoData
{
    class Program
    {
        static async Task Main(string[] args)
        {
            DisableConsoleQuickEdit.Go();

            var url = "https://api.binance.com/api/v3/exchangeInfo";

            var httpClient = HttpClientFactory.Create();

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, url);


            HttpResponseMessage httpResponse = await httpClient.SendAsync(httpRequest);

            if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine("Error at HttpResponse Pairs!!");
                return;
            }

            var content = await httpResponse.Content.ReadAsStringAsync();

            //var parsed = JObject.Parse(content);

            var token = JToken.Parse(content);
            var symbols = token.Value<JArray>("symbols");
            var count = symbols.Count;


            var tradingPairList = new List<TradingPair>();

            for (int i = 0; i < count; i++)
            {
                var tradingPair = new TradingPair();
                tradingPair.PairName = symbols[i].SelectToken($"symbol").Value<string>();
                tradingPairList.Add(tradingPair);
            }

            Console.WriteLine("             All trading pairs: ");
            Console.WriteLine("===========================================");

            for (int pairNum = 0; pairNum < count; pairNum++)
            {
                Console.WriteLine("                  " + tradingPairList[pairNum].PairName + "  ");
                if (pairNum % 10 == 8) Console.WriteLine();
            }

            Console.WriteLine("\n\n");
            /////////////////////////////////////////////////////////////////////////////

            //Console.Write("Enter trading pair (example BTCUSDT): ");

            //var inputString = Console.ReadLine();
            var inputString = "BTCUSDT";
            //bool isValid = false;
            //for (int pairNum = 0; pairNum < count; pairNum++)
            //{
            //    var checkString = parsed.SelectToken($"symbols[{pairNum}].symbol").Value<string>();
            //    if (checkString == inputString)
            //    {
            //        isValid = true;
            //        break;
            //    }
            //}

            //if (isValid == false)
            //{
            //    Console.WriteLine("Entered unexisting pair!");
            //    return;
            //}

            /////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("\n\n                            Recent Trades: ");
            Console.WriteLine("======================================================================");
            Console.WriteLine("||    Pair    ||     Price     ||     Amount      ||     Time     ||");
            Console.WriteLine("======================================================================");

            var limit = "&limit=2";

            var urlTrades = "https://api.binance.com/api/v3/trades?symbol=" + inputString + limit;

            while (true)
            {
                var httpRequestTrades = new HttpRequestMessage(HttpMethod.Get, urlTrades);
                HttpResponseMessage httpResponseTrades = await httpClient.SendAsync(httpRequestTrades);

                if (httpResponseTrades.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Console.WriteLine("Error at HttpResponse Traids!!");
                    return;
                }

                var contentTwo = await httpResponseTrades.Content.ReadAsStringAsync();

                var parsedTwo = JArray.Parse(contentTwo);
                var countTwo = parsedTwo.Count;

                var recentTradeList = new List<RecentTrade>();

                for (int i = 0; i < countTwo; i++)
                {
                    var recentTrade = new RecentTrade();

                    recentTrade.pair = inputString;
                    recentTrade.price = parsedTwo[i].SelectToken("price").Value<double>();
                    recentTrade.amount = parsedTwo[i].SelectToken("qty").Value<double>();
                    recentTrade.time = parsedTwo[i].SelectToken("time").Value<long>();
                    recentTrade.isBuyerMaker = parsedTwo[i].SelectToken("isBuyerMaker").Value<bool>();

                    recentTradeList.Add(recentTrade);
                }

                DateTimeOffset dateTimeOffset;

                for (int i = 0; i < countTwo; i++)
                {
                    string pair_str = recentTradeList[i].pair;
                    string price_str = String.Format("{0:F2}", recentTradeList[i].price);
                    string amount_str = String.Format("{0:F5}", recentTradeList[i].amount);

                    dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(recentTradeList[i].time);
                    string time_str = dateTimeOffset.LocalDateTime.ToLongTimeString();

                    Console.Write($"||   {pair_str}  ||    ");

                    if (recentTradeList[i].isBuyerMaker == true)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }

                    Console.Write($"{price_str}");


                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"   ||     {amount_str}     ||" + $"   {time_str}  ||");

                    Console.WriteLine("======================================================================");
                }
            }
        }
    }
}
